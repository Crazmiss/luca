package com.luca.crazmiss.luca.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.luca.crazmiss.luca.Data.Character;
import com.luca.crazmiss.luca.MainViewModel;
import com.luca.crazmiss.luca.R;

public class CharacterListAdapter extends ArrayAdapter<Character>  {


    private final MainViewModel mainViewModel;

    public CharacterListAdapter(Context context, MainViewModel viewModel) {
        super(context, R.layout.item_character_main, viewModel.getList());
        this.mainViewModel = viewModel;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        ListViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_character_main, parent, false);
            viewHolder = new ListViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ListViewHolder) convertView.getTag();
        }

        if (position > getCount()) {
            return convertView;
        }

        // Get the data item for this position
        Character character = getItem(position);

        if (character != null) {
            final String characterId = character.getId();

            viewHolder.tvName.setText(character.getName());
            viewHolder.tvBirthYear.setText(character.getBirthYear());

            viewHolder.tbFavourite.setButtonDrawable(mainViewModel.isFavCharacter(characterId) ? android.R.drawable.star_on : android.R.drawable.star_off);
            viewHolder.tbFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.isShown()) {
                        mainViewModel.changeCharacterIsFavourite(characterId);
                        notifyDataSetChanged();
                    }
                }
            });

        }
        // Return the completed view to render on screen
        return convertView;
    }



    class ListViewHolder {
        TextView tvName;
        TextView tvBirthYear;
        ToggleButton tbFavourite;

        ListViewHolder(View base) {
            tvName = base.findViewById(R.id.textview_character);
            tvBirthYear = base.findViewById(R.id.textview_birthyear);
            tbFavourite = base.findViewById(R.id.toggleButton);


        }

    }


}



