package com.luca.crazmiss.luca.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.luca.crazmiss.luca.Data.Film;
import com.luca.crazmiss.luca.Data.Vehicle;
import com.luca.crazmiss.luca.R;

import java.util.ArrayList;

public class ListAdapter<T> extends ArrayAdapter<T> {

    public ListAdapter(Context context, int layout, ArrayList<T> objArray) {
        super(context, layout, objArray);
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ListViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_obj_character, parent, false);
            viewHolder = new ListViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ListViewHolder) convertView.getTag();
        }

        if (position > getCount()) {
            return convertView;
        }

        // Get the data item for this position
        T obj = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view

        if (obj != null) {
            if (obj instanceof Film) {
                viewHolder.tvTitle.setText(((Film) obj).getTitle());
            }
            if (obj instanceof Vehicle) {
                viewHolder.tvTitle.setText(((Vehicle) obj).getName());
            }

        }
        // Return the completed view to render on screen
        return convertView;
    }


    private class ListViewHolder {
        TextView tvTitle;

        ListViewHolder(View base) {
            tvTitle = base.findViewById(R.id.textview_name);
        }

    }
}




