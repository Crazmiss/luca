package com.luca.crazmiss.luca.Api;

import com.luca.crazmiss.luca.Constants.ApiStatusEnum;

public interface RequestCallback {
    void onDone(boolean success, ApiStatusEnum result, boolean hasNext);
}
