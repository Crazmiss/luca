package com.luca.crazmiss.luca.Api;

import android.support.annotation.NonNull;

import com.luca.crazmiss.luca.Constants.ApiConstants;
import com.luca.crazmiss.luca.Data.Character;
import com.luca.crazmiss.luca.Data.Film;
import com.luca.crazmiss.luca.Data.Planet;
import com.luca.crazmiss.luca.Data.Vehicle;
import com.luca.crazmiss.luca.Storage.CharacterStorage;
import com.luca.crazmiss.luca.Storage.FilmStorage;
import com.luca.crazmiss.luca.Storage.VehicleStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.luca.crazmiss.luca.Constants.ApiStatusEnum.FAILED;
import static com.luca.crazmiss.luca.Constants.ApiStatusEnum.SUCCESS;

public class StarWarsApi {


    public StarWarsApi() {

    }

    public void requestAllCharacters(final RequestCallback requestCallback) {

        String url = ApiConstants.BASE_URL + ApiConstants.PEOPLE_API;

        for (int i = 1; i < 10; i++) {
            retrievePeopleByUrl(url, i, requestCallback);
        }
        requestCallback.onDone(true, SUCCESS, false);


    }

    private void retrievePeopleByUrl(String urlString, int pageNum, final RequestCallback requestCallback) {

        OkHttpClient client = new OkHttpClient();

        try {
            URL url = new URL(urlString + "?page=" + pageNum);


            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    requestCallback.onDone(false, FAILED, false);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    ResponseBody responseBody = response.body();

                    if (!response.isSuccessful() || responseBody == null) {
                        requestCallback.onDone(false, FAILED, false);
                    }

                    assert responseBody != null;
                    String result = responseBody.string();

                    if (result != null) {
                        try {
                            JSONObject people = new JSONObject(result);

                            JSONArray contacts = people.getJSONArray("results");
                            boolean hasNext = people.getString("next") != null;
                            int count = people.getInt("count");

                            for (int i = 0; contacts.length() > i; i++) {
                                JSONObject characterObj = contacts.getJSONObject(i);
                                Character character = retrieveCharacterFromJson(characterObj);
                                if (character != null) {
                                    CharacterStorage.add(character);
                                }
                            }
                            requestCallback.onDone(true, SUCCESS, count != CharacterStorage.getList().size() );

                        } catch (JSONException e) {
                            e.printStackTrace();
                            requestCallback.onDone(true, FAILED, false);

                        }
                    }

                }
            });

        } catch (MalformedURLException e) {
            e.printStackTrace();
            requestCallback.onDone(true, FAILED, false);

        }

    }

    public void retrieveFilms(String id, final RequestCallback requestCallback) {

        OkHttpClient client = new OkHttpClient();

        try {
            URL url = new URL(id);


            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    requestCallback.onDone(false, FAILED, false);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    ResponseBody responseBody = response.body();

                    if (!response.isSuccessful() || responseBody == null) {
                        requestCallback.onDone(false, FAILED, false);
                    }

                    assert responseBody != null;
                    String result = responseBody.string();

                    if (result != null) {
                        try {
                            JSONObject filmInformation = new JSONObject(result);
                            Film film = retrieveFilmsFromJson(filmInformation);
                            if (film != null) {
                                FilmStorage.add(film);
                            }

                            requestCallback.onDone(true, SUCCESS, false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            requestCallback.onDone(true, FAILED, false);

                        }
                    }

                }
            });

        } catch (MalformedURLException e) {
            e.printStackTrace();
            requestCallback.onDone(true, FAILED, false);

        }

    }


    public void retrieveVehicles(String id, final RequestCallback requestCallback) {

        OkHttpClient client = new OkHttpClient();

        try {
            URL url = new URL(id);


            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    requestCallback.onDone(false, FAILED, false);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    ResponseBody responseBody = response.body();

                    if (!response.isSuccessful() || responseBody == null) {
                        requestCallback.onDone(false, FAILED, false);
                    }

                    assert responseBody != null;
                    String result = responseBody.string();

                    if (result != null) {
                        try {
                            JSONObject vehicleInformation = new JSONObject(result);
                            Vehicle vehicle = retrieveVehiclesFromJson(vehicleInformation);
                            if (vehicle != null) {
                                VehicleStorage.add(vehicle);
                            }

                            requestCallback.onDone(true, SUCCESS, false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            requestCallback.onDone(true, FAILED, false);

                        }
                    }

                }
            });

        } catch (MalformedURLException e) {
            e.printStackTrace();
            requestCallback.onDone(true, FAILED, false);

        }

    }

    public void retrieveHomeWorld(final String characterId, String planetId, final RequestCallback requestCallback) {

        OkHttpClient client = new OkHttpClient();

        try {
            URL url = new URL(planetId);


            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    requestCallback.onDone(false, FAILED, false);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    ResponseBody responseBody = response.body();

                    if (!response.isSuccessful() || responseBody == null) {
                        requestCallback.onDone(false, FAILED, false);
                    }

                    assert responseBody != null;
                    String result = responseBody.string();

                    if (result != null) {
                        try {
                            JSONObject vehicleInformation = new JSONObject(result);
                            Planet planet = retrievePlanetFromJson(vehicleInformation);
                            if (planet != null) {
                                Objects.requireNonNull(CharacterStorage.getCharacter(characterId)).setHomePlanet(planet);
                            }

                            requestCallback.onDone(true, SUCCESS, false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            requestCallback.onDone(true, FAILED, false);

                        }
                    }

                }
            });

        } catch (MalformedURLException e) {
            e.printStackTrace();
            requestCallback.onDone(true, FAILED, false);

        }

    }


    private Character retrieveCharacterFromJson(JSONObject characterObj) {

        try {
            // retrieving person information about the character
            String id = characterObj.getString("url");
            String name = characterObj.getString("name");
            String birthYear = characterObj.getString("birth_year");

            Character character = new Character(id, name, birthYear);

            //retrieve planet information for homeplanet
            String planetId = characterObj.getString("homeworld");
            Planet planet = new Planet(planetId, "");
            character.setHomePlanet(planet);

            // retrieving film information about the character
            JSONArray filmsArray = characterObj.getJSONArray("films");

            ArrayList<String> filmIds = new ArrayList<>();
            for (int i = 0; filmsArray.length() > i; i++) {
                String filmId = filmsArray.getString(i);
                filmIds.add(filmId);
            }

            character.setFilms(filmIds);

            // Retrieve vehicle information about the character
            JSONArray vehiclesArray = characterObj.getJSONArray("vehicles");

            ArrayList<String> vehicleIds = new ArrayList<>();
            for (int i = 0; vehiclesArray.length() > i; i++) {
                String vehicleId = vehiclesArray.getString(i);
                vehicleIds.add(vehicleId);
            }

            character.setVehicles(vehicleIds);

            return character;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Film retrieveFilmsFromJson(JSONObject filmObj) {

        try {
            String id = filmObj.getString("url");
            String title = filmObj.getString("title");

            return new Film(id, title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Vehicle retrieveVehiclesFromJson(JSONObject vehicleInformation) {
        try {
            String id = vehicleInformation.getString("url");
            String name = vehicleInformation.getString("name");

            return new Vehicle(id, name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }

    private Planet retrievePlanetFromJson(JSONObject planetInformation) {
        try {
            String id = planetInformation.getString("url");
            String name = planetInformation.getString("name");

            return new Planet(id, name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
