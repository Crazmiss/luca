package com.luca.crazmiss.luca;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.luca.crazmiss.luca.Adapters.ListAdapter;
import com.luca.crazmiss.luca.Constants.ApiStatusEnum;
import com.luca.crazmiss.luca.Api.RequestCallback;
import com.luca.crazmiss.luca.Data.Character;
import com.luca.crazmiss.luca.Data.Film;
import com.luca.crazmiss.luca.Data.Vehicle;
import com.luca.crazmiss.luca.Functions.NetworkFunctions;
import com.luca.crazmiss.luca.Storage.CharacterStorage;

public class CharacterActivity extends AppCompatActivity {

    private CharacterViewModel viewModel;
    private ListAdapter<Film> filmAdapter;
    private ListAdapter<Vehicle> vehicleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        boolean networkAvailability = NetworkFunctions.isNetworkAvailable(getBaseContext());

        Bundle intentBundle = getIntent().getExtras();
        if (intentBundle != null) {
            String id = intentBundle.getString("characterId");

            Character character = CharacterStorage.getCharacter(id);
            if (networkAvailability) {

                viewModel = new CharacterViewModel();
                viewModel.init(character);
                viewModel.retrieveAllUnknown(new RequestCallback() {
                    @Override
                    public void onDone(boolean success, ApiStatusEnum result, boolean hasNext) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                refreshData();
                            }
                        });
                    }
                });

                filmAdapter = new ListAdapter<>(this, R.id.listview_films_characters, viewModel.getFilms());

                ListView filmList = findViewById(R.id.listview_films_characters);
                filmList.setAdapter(filmAdapter);

                vehicleAdapter = new ListAdapter<>(this, R.id.listview_vehicles_characters, viewModel.getVehicles());

                ListView vehicleList = findViewById(R.id.listview_vehicles_characters);
                vehicleList.setAdapter(vehicleAdapter);

                initUI();
            }
        }

    }

    private void refreshData() {
        filmAdapter.notifyDataSetChanged();
        vehicleAdapter.notifyDataSetChanged();
        initUI();
    }

    private void initUI() {


        TextView textViewName = findViewById(R.id.textview_name_characters);
        TextView textViewBirthyear = findViewById(R.id.textview_birthyear_characters);
        TextView textViewHomePlanet = findViewById(R.id.textview_homeplanet_characters);
        ImageView imageViewFavourite = findViewById(R.id.image_favourite);

        textViewName.setText(viewModel.getCharacterName());
        textViewBirthyear.setText(viewModel.getCharacterBirthYear());
        textViewHomePlanet.setText(viewModel.getCharacterHomeWorld());
        imageViewFavourite.setImageResource(viewModel.isCharacterFavourite()?android.R.drawable.star_on:android.R.drawable.star_off);


    }

}
