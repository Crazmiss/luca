package com.luca.crazmiss.luca;

import com.luca.crazmiss.luca.Constants.ApiStatusEnum;
import com.luca.crazmiss.luca.Api.RequestCallback;
import com.luca.crazmiss.luca.Api.StarWarsApi;
import com.luca.crazmiss.luca.Data.Character;
import com.luca.crazmiss.luca.Data.Film;
import com.luca.crazmiss.luca.Data.Vehicle;
import com.luca.crazmiss.luca.Storage.CharacterStorage;
import com.luca.crazmiss.luca.Storage.FilmStorage;
import com.luca.crazmiss.luca.Storage.VehicleStorage;

import java.util.ArrayList;

class CharacterViewModel {


    private Character character;
    private ArrayList<Film> films = new ArrayList<>();
    private ArrayList<Vehicle> vehicles = new ArrayList<>();

    void init(Character character) {
        this.character = character;
    }

    void retrieveAllUnknown(final RequestCallback requestCallback) {
        StarWarsApi starWarsApi = new StarWarsApi();
        films.clear();
        vehicles.clear();

        //retrieve all films
        for (int i = 0; i < character.getFilms().size(); i++) {
            String id = character.getFilms().get(i);

            starWarsApi.retrieveFilms(id, new RequestCallback() {
                @Override
                public void onDone(boolean success, ApiStatusEnum result, boolean hasNext) {
                    films = FilmStorage.getCharacterFilms(character.getFilms());
                    requestCallback.onDone(success, result, hasNext);

                }
            });
        }

        //retrieve all vehicles
        for (int i = 0; i < character.getVehicles().size(); i++) {
            String id = character.getVehicles().get(i);

            starWarsApi.retrieveVehicles(id, new RequestCallback() {
                @Override
                public void onDone(boolean success, ApiStatusEnum result, boolean hasNext) {
                    vehicles = VehicleStorage.getCharacterVehicles(character.getVehicles());
                    requestCallback.onDone(success, result, hasNext);

                }
            });
        }

        starWarsApi.retrieveHomeWorld(character.getId(), character.getHomePlanet().getId(), new RequestCallback() {
            @Override
            public void onDone(boolean success, ApiStatusEnum result, boolean hasNext) {
                String characterId = character.getId();
                character = CharacterStorage.getCharacter(characterId);
                requestCallback.onDone(success, result, hasNext);

            }
        });

    }


    String getCharacterName() {
        return character.getName();
    }

    String getCharacterBirthYear() {
        return character.getBirthYear();
    }

    String getCharacterHomeWorld() {
        return character.getHomePlanet().getName();
    }


    ArrayList<Film> getFilms() {
        films = FilmStorage.getCharacterFilms(character.getFilms());
        return films;
    }

    ArrayList<Vehicle> getVehicles() {
        vehicles = VehicleStorage.getCharacterVehicles(character.getVehicles());
        return vehicles;
    }

    public boolean isCharacterFavourite() {
        return CharacterStorage.inFavList(character.getId());
    }
}
