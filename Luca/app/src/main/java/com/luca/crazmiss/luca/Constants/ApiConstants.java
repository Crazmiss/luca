package com.luca.crazmiss.luca.Constants;

public class ApiConstants {

    public static final String BASE_URL = "https://swapi.co/api/";
    public static final String PEOPLE_API = "people/";

}
