package com.luca.crazmiss.luca.Constants;


public enum ApiStatusEnum {

    FAILED,
    SUCCESS,
    NETWORK_ERROR
}
