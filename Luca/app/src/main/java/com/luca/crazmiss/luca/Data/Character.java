package com.luca.crazmiss.luca.Data;

import java.util.ArrayList;

public class Character extends Person {

    private ArrayList<String> filmIds = new ArrayList<>();
    private ArrayList<String> vehicleIds = new ArrayList<>();
    private Planet homePlanetId;

    public Character(String id, String name, String birthyear) {
        super(id, name, birthyear);
    }

    public ArrayList<String> getFilms() {
        return filmIds;
    }

    public void setFilms(ArrayList<String> films) {
        this.filmIds = films;
    }

    public ArrayList<String> getVehicles() {
        return vehicleIds;
    }

    public void setVehicles(ArrayList<String> vehicles) {
        this.vehicleIds = vehicles;
    }

    public Planet getHomePlanet() {
        return homePlanetId;
    }

    public void setHomePlanet(Planet planet) {
        this.homePlanetId = planet;
    }

}
