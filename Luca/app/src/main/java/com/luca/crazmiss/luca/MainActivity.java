package com.luca.crazmiss.luca;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.luca.crazmiss.luca.Adapters.CharacterListAdapter;
import com.luca.crazmiss.luca.Api.RequestCallback;
import com.luca.crazmiss.luca.Constants.ApiStatusEnum;

public class MainActivity extends AppCompatActivity {
    private CharacterListAdapter adapter;

    private MainViewModel viewModel;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getBaseContext();
        viewModel = new MainViewModel();
        adapter = new CharacterListAdapter(context, viewModel);

        ListView list = findViewById(R.id.listview_characters_main);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, CharacterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("characterId", viewModel.getList().get(position).getId());
                getBaseContext().startActivity(intent);
            }
        });

        viewModel = new MainViewModel();
        viewModel.retrieveAllUnknown(new RequestCallback() {
            @Override
            public void onDone(final boolean success, ApiStatusEnum result, final boolean hasNext) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (success && !hasNext) {
                            initUI();
                        }
                    }
                });
            }
        });

        initUI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items, menu);
        menu.findItem(R.id.action_sort_order).setTitle(viewModel.getSortOrderTitle(this));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sort_order) {
            viewModel.changeSortOrder(viewModel.getList());

            item.setTitle(viewModel.getSortOrderTitle(this));

            initUI();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        viewModel.getSortedCharacters();
        adapter.notifyDataSetChanged();

    }


}
