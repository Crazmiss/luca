package com.luca.crazmiss.luca;

import android.content.Context;

import com.luca.crazmiss.luca.Constants.ApiStatusEnum;
import com.luca.crazmiss.luca.Api.RequestCallback;
import com.luca.crazmiss.luca.Api.StarWarsApi;
import com.luca.crazmiss.luca.Data.Character;
import com.luca.crazmiss.luca.Data.Settings;
import com.luca.crazmiss.luca.Storage.CharacterStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainViewModel {

    private static Settings sortOrder = Settings.NAME_ASC;

    void retrieveAllUnknown(final RequestCallback requestCallback) {
        if (this.getList().size() == 0) {
            StarWarsApi starWarsApi = new StarWarsApi();
            starWarsApi.requestAllCharacters(new RequestCallback() {
                @Override
                public void onDone(boolean success, ApiStatusEnum result, boolean hasNext) {
                    requestCallback.onDone(success, result, hasNext);
                }
            });
        }

    }


    void getSortedCharacters() {
        sortList();
    }

    private void sortList() {

        ArrayList<Character> c = this.getList();
        if (c != null && c.size() != 0) {
            Collections.sort(c, new Comparator<Character>() {
                @Override
                public int compare(Character rPerson, Character lPerson) {
                    if (rPerson != null && lPerson != null) {
                        if (sortOrder == Settings.BIRTHYEAR_ASC || sortOrder == Settings.BIRTHYEAR_DESC) {

                            if (lPerson.getNumericBirthYear() > rPerson.getNumericBirthYear()) {
                                return sortOrder == Settings.BIRTHYEAR_ASC ? -1 : 1;
                            } else {
                                if (lPerson.getNumericBirthYear() < rPerson.getNumericBirthYear()) {
                                    return sortOrder == Settings.BIRTHYEAR_ASC ? 1 : -1;
                                } else {
                                    return 0;
                                }
                            }
                        }
                        if (sortOrder == Settings.NAME_ASC) {
                            return rPerson.getName().compareTo(lPerson.getName());
                        } else {
                            return lPerson.getName().compareTo(rPerson.getName());
                        }
                    }
                    return 0;
                }
            });
        }
    }

    void changeSortOrder(ArrayList<Character> characters) {

        if (sortOrder == Settings.NAME_ASC) {
            sortOrder = Settings.BIRTHYEAR_ASC;
        } else {
            sortOrder = Settings.NAME_ASC;
        }

        sortList();

    }

    String getSortOrderTitle(Context context) {
        switch (sortOrder) {
            // Since you want to read how the next sort would be you need to show the name f the other sorting way
            case BIRTHYEAR_ASC:
                return context.getString(R.string.sort_by_name);
            case NAME_ASC:
                return context.getString(R.string.sort_by_birthyear);
            default:
                return context.getString(R.string.item_sort);
        }
    }

    public void changeCharacterIsFavourite(String characterId) {
        if (CharacterStorage.inFavList(characterId)) {
            CharacterStorage.removeFavourite(characterId);
        } else {
            CharacterStorage.addToFavList(characterId);

        }
    }

    public boolean isFavCharacter(String characterId) {
        return CharacterStorage.inFavList(characterId);
    }

    public ArrayList<Character> getList() {
        return CharacterStorage.getList();
    }
}
