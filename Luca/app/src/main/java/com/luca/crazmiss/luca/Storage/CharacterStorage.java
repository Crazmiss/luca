package com.luca.crazmiss.luca.Storage;

import com.luca.crazmiss.luca.Data.Character;

import java.util.ArrayList;
import java.util.Hashtable;

public class CharacterStorage {

    private static Hashtable<String, Character> characterHashtable = new Hashtable<>();
    private static ArrayList<Character> characters = new ArrayList<>();
    private static ArrayList<String> favouriteCharacters = new ArrayList<>();

    public static void add(Character character) {
        characterHashtable.put(character.getId(), character);
        update();
    }

    private static void update() {
        characters.clear();
        characters.addAll(characterHashtable.values());
    }

    public static ArrayList<Character> getList() {
        return characters;
    }

    public static ArrayList<String> getFavouriteList() {
        return favouriteCharacters;
    }


    public static void addToFavList(String characterId) {
        favouriteCharacters.add(characterId);
    }

    public static boolean inFavList(String characterId) {
        for (int i = 0; i < favouriteCharacters.size(); i++) {
            if (favouriteCharacters.get(i).equals(characterId)) {
                return true;
            }
        }
        return false;
    }

    public static void clearFavouriteList() {
        favouriteCharacters.clear();
    }

    public static void removeFavourite(String characterId) {
        if (favouriteCharacters.size() == 0) return;
        int i;

        for (i = 0; i < favouriteCharacters.size(); i++) {
            if (favouriteCharacters.get(i).equals(characterId)) {
                break;
            }
        }

        favouriteCharacters.remove(i);
    }


    public static Character getCharacter(String id) {

        if (characterHashtable.containsKey(id)) {
            return characterHashtable.get(id);
        } else {
            return null;
        }
    }


}
