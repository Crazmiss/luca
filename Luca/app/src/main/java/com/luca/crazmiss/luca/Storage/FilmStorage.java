package com.luca.crazmiss.luca.Storage;

import com.luca.crazmiss.luca.Data.Film;

import java.util.ArrayList;
import java.util.Hashtable;

public class FilmStorage {

    private static Hashtable<String, Film> filmsHashtable = new Hashtable<>();
    private static ArrayList<Film> films = new ArrayList<>();
    private static ArrayList<Film> currentSetOfFilms = new ArrayList<>();

    public static void add(Film film){
        filmsHashtable.put(film.getId(), film);
        update();
    }

    private static void update(){
        films.clear();
        films.addAll(filmsHashtable.values());
    }

    public static ArrayList<Film> getList(){
        return films;
    }

    public static Film getFilm(String id){

        if(filmsHashtable.containsKey(id)){
            return filmsHashtable.get(id);
        }else {
            return null;
        }
    }

    public static ArrayList<Film> getCharacterFilms(ArrayList<String> filmIds){

        currentSetOfFilms.clear();
        for(int i=0; i< filmIds.size(); i++){

            String id = filmIds.get(i);

            if(filmsHashtable.containsKey(id)){
                currentSetOfFilms.add(filmsHashtable.get(id));
            }
        }

        return currentSetOfFilms;
    }


}
