package com.luca.crazmiss.luca.Storage;

import com.luca.crazmiss.luca.Data.Vehicle;

import java.util.ArrayList;
import java.util.Hashtable;

public class VehicleStorage {

    private static Hashtable<String, Vehicle> vehiclesHashtable = new Hashtable<>();
    private static ArrayList<Vehicle> vehicles = new ArrayList<>();
    private static ArrayList<Vehicle> currentSetOfVehicles = new ArrayList<>();

    public static void add(Vehicle vehicle){
        vehiclesHashtable.put(vehicle.getId(), vehicle);
        update();
    }

    private static void update(){
        vehicles.clear();
        vehicles.addAll(vehiclesHashtable.values());
    }

    public static ArrayList<Vehicle> getList(){
        return vehicles;
    }

    public static Vehicle getFilm(String id){

        if(vehiclesHashtable.containsKey(id)){
            return vehiclesHashtable.get(id);
        }else {
            return null;
        }
    }

    public static ArrayList<Vehicle> getCharacterVehicles(ArrayList<String> filmIds){

        currentSetOfVehicles.clear();
        for(int i=0; i< filmIds.size(); i++){

            String id = filmIds.get(i);

            if(vehiclesHashtable.containsKey(id)){
                currentSetOfVehicles.add(vehiclesHashtable.get(id));
            }
        }

        return currentSetOfVehicles;
    }


}
