# Read me
This is a project for retrieving famous lucas films characters and some of their information.

## What is in the basic implementation?
* Get List of characters from [swapi-starwars](https://swapi.co/api/people) API.
* Show a list of characters sorted by default on name.
* Add one button that allows the use to change the sort method between:
  * Character name
  * Character birth year
* Add one button to each cell to mark characters as favourite.
* If you press the cell you have to show another view with the character detailed Information:
  * Films
  * Vehicles
  * Homeworld

## Extras
Extras to implement
* Search character
* Show list of favourite characters
* Offline availability

## What got implemented?
The main points have been implement and due to time reasons not the extras.

## Clarifications
1. API  
  The wish was to sort the characters by name and show them. Therefore the assumption was made that all the characters needed to be retrieved at the start.
  The API allows us to retrieve the character information paginated, which means the response of the request contains a `next` part. This next part indicates that there is more information to retrieve.
  The problem was that the pages that could be retrieved are not sorted and cannot be sorted. If we tried to sort each page's results, then each next page's results will appear in between the previous ones. For example, page 1 has items `g,a,j` so we sort then as `a,g,j`. Page 2 has items `f,d,z` so when we add them and sort, we get `a,d,f,g,j,z`. This kind of addition to a list feels very strange to the user. Pagination and sorting only work well if the backend supports it. To overcome this problem, it was necessary to retrieve all the characters first and sort them all at once. To do so, and ensure that the requests are still done in the background, an approximation was done to call a number of pages and see what information can be retrieved. This is not the ideal way, but because this is a mostly static API, you will not encounter the problem of not getting all the characters quickly and up-front.

  How this could have been avoided:
  * The backend would have the option of retrieving the information sorted. This would have changed the implementation in the way to retrieve the information page by page and not all at once.
  * Talking to the client about their wishes. It could be that changing the backend is not possible or slow. Then we need to talk about maybe removing the sorting feature, or compromise in the paging, in order to have sorting.
  * Lastly, I personally think there might have been a way to retrieve all people and sort them using the next mechanism, but I couldn't think of one with keeping the time limit in mind.


2. Data and storage  
  I split up the different categories in different Storages. This was done because multiple Characters share the same Film, Vehicle or Homeworld. Instead of taking space for each characters private information, one big storage for each category was created, which is shared.
  The characters connect to the storage by id which is the url of a film/vehicle/planet, where you would be able to retrieve the information of e.g. the vehicle of that character.

    The characters themselves don't contain the information if they are favourite or not. This information is stored as an arraylist in the character storage. This was done to ensure that we will not lose the favourite information when updating/retrieving the character again.

3. Sort button  
  I put the sort button on the dot menu because it allowed me to write some text about the sorting field, where a simple icon button wouldn't have been as clear.

## Ideas to resolve the other wishes

1. Searching  
  For searching, the API provides the option to search for a specific character. This system would have been used to create the search mechanism.

2. Favourite view  
  Most of the information necessary to show only the favourite characters have already been implemented. I would create a button on the top that toggles between showing all and showing only favourited characters.

3. Offline  
  To make the app work offline, it is necessary to save the information on the device. This could be done after initially retrieving the information. If we have network availability, the information would always be updated and saved again. This is done to ensure that most of the information is available again when going offline.  
  Since the extra information (films, Vehicles, etc.) need to be queried as well, it could be that a person doesn't have all the information to successfully show all the information. To indicate this to the user, it would be possible to show a little information on the screen like "missing 2 elements".

4. Refresh information  
  To be able to refresh information, when the app is already running, a swipe to refresh layout should be implemented.

## Other information

I was trying to keep the view and the data separate from each other by creating a viewmodel to connect those two.  
I would have liked to have more time to use databinding and retrofit. Since I had a limited amount of time, I choose to use the libs I already knew.  
I would like to learn more about them and use them in my future projects.
